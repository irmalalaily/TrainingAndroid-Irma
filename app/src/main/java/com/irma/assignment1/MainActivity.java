package com.irma.assignment1;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.irma.assignment1.taks.PostActivityTask;

public class MainActivity extends AppCompatActivity {
    TextView userInputView;
    Button buttonDetailView;
    Button buttonShowList;
    String userInput;
    String passInput;
    String detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userInputView = (TextView) findViewById(R.id.usernameInput);
        buttonDetailView = (Button) findViewById(R.id.buttonDetail);
        buttonShowList = (Button) findViewById(R.id.buttonList);

        userInput = getIntent().getStringExtra("user");
        passInput = getIntent().getStringExtra("pass");

        userInputView.setText(userInput + "!!");

        detail = "Username: " + userInput + "\nPassword: " + passInput;

        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.detail_akun_title).setMessage(detail);

        buttonDetailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.create().show();
            }
        });

        buttonShowList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PostActivityTask(MainActivity.this).execute(passInput, userInput);

                Log.d("TAG", "Selesai");
            }
        });
    }
}

