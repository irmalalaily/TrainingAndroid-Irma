package com.irma.assignment1.service;

import com.irma.assignment1.model.ListMenuItem;
import com.irma.assignment1.model.Post;
import com.irma.assignment1.model.Repo;
import com.irma.assignment1.model.UserEmail;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by irma.latifatul on 15/06/17.
 */

public interface PostService {
    @GET("/posts")
    Call<List<Post>> listAll();

    @GET("/v2/{param}")
    Call<List<ListMenuItem>> listAllMenu(@Path("param") String param);

    @GET("/users/{username}/repos")
    Call<List<Repo>> listAllRepository(@Path("username") String username);

    @GET("/user/emails")
    Call<List<UserEmail>> listAllEmail(@Header("Authorization") String token);
}
