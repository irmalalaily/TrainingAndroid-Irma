package com.irma.assignment1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irma.assignment1.adapter.ListMenuAdapter;
import com.irma.assignment1.helper.RequestHelper;
import com.irma.assignment1.model.ListMenuItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by irma.latifatul on 14/06/17.
 */

public class MenulistActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RequestHelper requestHelper = new RequestHelper(getApplicationContext(), getString(R.string.api_url));
        setContentView(R.layout.activity_menulist);

        //loading dynamic menu with adapter
        /*
        ListView listView = (ListView) findViewById(R.id.list_menu);
        String jsonMenu = getIntent().getStringExtra("myMenu");
        ArrayList<ListMenuItem> listMenuItems = new Gson().fromJson(jsonMenu, new TypeToken<List<ListMenuItem>>(){}.getType());
        ListMenuAdapter listMenuAdapter = new ListMenuAdapter(MenulistActivity.this, R.layout.list_menu_layout, listMenuItems);
        listView.setAdapter(listMenuAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MenulistActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
            }
        });
        */

        TextView resp = (TextView) findViewById(R.id.backend_response);
        resp.setText(getIntent().getStringExtra("myRepo"));

    }

    private ArrayList<ListMenuItem> createSampleMenu() {
        ListMenuItem item1 = new ListMenuItem(1, "https://image.flaticon.com/icons/png/512/148/148992.png", "Mata Kuliah1", "3 SKS, Term 2");
        ListMenuItem item2 = new ListMenuItem(1, "https://image.flaticon.com/icons/png/512/148/148992.png", "Mata Kuliah2", "4 SKS, Term 5");
        ListMenuItem item3 = new ListMenuItem(1, "https://image.flaticon.com/icons/png/512/148/148992.png", "Mata Kuliah3", "6 SKS, Term 5");
        ListMenuItem item4 = new ListMenuItem(1, "https://image.flaticon.com/icons/png/512/148/148992.png", "Mata Kuliah4", "4 SKS, Term 6");
        ArrayList<ListMenuItem> list = new ArrayList<ListMenuItem>();
        list.add(item1);
        list.add(item2);
        list.add(item3);
        list.add(item4);

        return list;
    }
}
