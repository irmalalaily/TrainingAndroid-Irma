package com.irma.assignment1;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    EditText username;
    EditText password;
    Button loginButton;
    Context context;
    String namaPengguna;
    String kataSandi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        loginButton = (Button) findViewById(R.id.buttonLogin);

        context = getApplicationContext();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

    }

    public void login(){
        namaPengguna = username.getText().toString();
        kataSandi = password.getText().toString();

        if (isValid()){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("user", namaPengguna);
            intent.putExtra("pass", kataSandi);
            context.startActivity(intent);
            Toast.makeText(this, R.string.login_berhasil, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.login_gagal, Toast.LENGTH_SHORT).show();
            return;
        }

    }

    public boolean isValid() {
        boolean valid = true;

        if (namaPengguna.isEmpty()) {
            valid = false;
            username.setError("Masukkan Username!");
        } else {
            username.setError(null);
        }

        if (kataSandi.isEmpty()) {
            valid = false;
            password.setError("Masukkan Password!");
        } else {
            if (!kataSandi.equals("312213ill")) {
                valid = false;
                password.setError("Password Salah!");
            } else {
                password.setError(null);
            }
        }

        return valid;
    }
}
